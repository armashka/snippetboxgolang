# Build executable stage
FROM golang
ADD . /app
WORKDIR /app
RUN go build /app/cmd/web/
ENTRYPOINT /app/cmd/web/
# Build final image
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /app/app/cmd/web/ .
CMD [".app"]