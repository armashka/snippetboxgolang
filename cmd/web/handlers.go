package main

import (
	"armanbimak27/pkg/forms"
	"armanbimak27/pkg/models"
	"errors"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

func (app *application) home(w http.ResponseWriter, r *http.Request)  {
	s, err := app.snippets.Latest()
	if err != nil {
		app.ServerError(w, err)
		return
	}
	app.render(w, r, "home.page.tmpl", &TemplateData{
		Snippets: s,
		Snippet: nil,
	})

}

func (app *application) showSnippet(w http.ResponseWriter, r *http.Request)  {
	id := chi.URLParam(r, "id")
	intid, err := strconv.Atoi(id)
	if id == "" || err != nil || intid < 1{
		app.notFound(w)
		return
	}
	s, err := app.snippets.Get(intid)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.errorLog.Println(err)
			app.notFound(w)
		} else {
			app.ServerError(w, err)
		}
		return
	}


	templateDataa := &TemplateData{Snippet: s}
	app.render(w, r, "show.page.tmpl", templateDataa)
//TODO - Fprintf() read
//TODO - http status codes

}

func (app *application) createSnippet(w http.ResponseWriter, r *http.Request)  {


	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("title", "content", "expires")
	form.MaxLength("title", 100)
	form.PermittedValues("expires", "365", "7", "1")

	if !form.Valid() {
		app.render(w, r, "create.page.tmpl", &TemplateData{Form: form})
		return
	}

	expires, _ := strconv.Atoi(form.Get("expires"))

	id, err := app.snippets.Insert(form.Get("title"), form.Get("content"), expires)
	if err != nil {
		app.ServerError(w, err)
		return
	}

	app.session.Put(r, "flash", "Snippet successfully created!")

	http.Redirect(w, r, fmt.Sprintf("/snippet/%d", id), http.StatusSeeOther)
}


func (app *application) createSnippetForm(w http.ResponseWriter, r *http.Request) {


	// I had nil pointer exception. That is why I initialized Form struct to handle this problem.
	// My <form></form> in create.page.tmpl was not rendered.


	form := forms.New(r.PostForm)
	tmp := TemplateData{
		Form: form,
	}


	app.render(w, r, "create.page.tmpl", &tmp)
}


func (app *application) signupUserForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "signup.page.tmpl", &TemplateData{
		Form: forms.New(nil) })
}
func (app *application) signupUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}


	form := forms.New(r.PostForm)
	form.Required("name", "email", "password")
	form.MaxLength("name", 255)
	form.MaxLength("email", 255)
	form.MatchesPattern("email", forms.EmailRX)
	form.MinLength("password", 10)


	if !form.Valid() {
		app.render(w, r, "signup.page.tmpl", &TemplateData{Form: form})
		return
	}



	err = app.users.Insert(form.Get("name"), form.Get("email"), form.Get("password"))
	if err != nil {
		if errors.Is(err, models.ErrDuplicateEmail) {
			form.Errors.Add("email", "Address is already in use")
			app.render(w, r, "signup.page.tmpl", &TemplateData{Form: form})
		} else {
			app.ServerError(w, err)
		}
		return
	}
	app.session.Put(r, "flash", "Your signup was successful. Please log in.")
	http.Redirect(w, r, "/user/login", http.StatusSeeOther)

}


func (app *application) loginUserForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "login.page.tmpl", &TemplateData{
	Form: forms.New(nil) })
}


func (app *application) loginUser(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	id, err := app.users.Authenticate(form.Get("email"), form.Get("password"))
	if err != nil {
		if errors.Is(err, models.ErrInvalidCredentials) {
			form.Errors.Add("generic", "Email or Password is incorrect")
			app.render(w, r, "login.page.tmpl", &TemplateData{Form: form}) } else {
			app.ServerError(w, err) }
		return
	}

	app.session.Put(r, "authenticatedUserID", id)

	http.Redirect(w, r, "/snippet/create", http.StatusSeeOther)
}


func (app *application) logoutUser(w http.ResponseWriter, r *http.Request) {
	app.session.Remove(r, "authenticatedUserID")

	app.session.Put(r, "flash", "You've been logged out successfully!")
	http.Redirect(w, r, "/", http.StatusSeeOther)
}