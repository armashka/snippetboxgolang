package main

import (
	postgresql "armanbimak27/pkg/models/postgresql"
	"context"
	"crypto/tls"
	"flag"
	"github.com/go-chi/chi"
	"github.com/golangcollege/sessions"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/justinas/alice"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

)



type application struct {
	errorLog 			*log.Logger
	infoLog 			*log.Logger
	snippets 			*postgresql.SnippetModel
	templateCache 		map[string]*template.Template
	session 			*sessions.Session
	users 				*postgresql.UserModel
}



func (app *application) routes() http.Handler {

	standardMiddleware := alice.New(app.recoverPanic, app.logRequest, secureHeaders)
	dynamicMiddleware := alice.New(app.session.Enable)


	mux := chi.NewRouter()

	mux.Method("GET", "/", dynamicMiddleware.ThenFunc(app.home))
	mux.Method("GET", "/snippet/create", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.createSnippetForm))
	mux.Method("POST", "/snippet/create", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.createSnippet))
	mux.Method("GET", "/snippet/{id}", dynamicMiddleware.ThenFunc(app.showSnippet) )

	mux.Method("GET", "/user/signup", dynamicMiddleware.ThenFunc(app.signupUserForm))
	mux.Method("POST", "/user/signup", dynamicMiddleware.ThenFunc(app.signupUser))
	mux.Method("GET", "/user/login", dynamicMiddleware.ThenFunc(app.loginUserForm))
	mux.Method("POST", "/user/login",  dynamicMiddleware.ThenFunc(app.loginUser) )
	mux.Method("POST","/user/logout", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.logoutUser))


	workDir, _ := os.Getwd()
	filesDir := http.Dir(filepath.Join(workDir, "ui/static"))
	FileServer(mux, "/static/", filesDir)
	//fileServer := http.FileServer(http.Dir("./ui/static/"))
	//mux.Handle("/static/", http.StripPrefix("/static", fileServer))
	return  standardMiddleware.Then(mux)
}

func OpenDB(dsn string) (*pgxpool.Pool, error)  {
	dbpool, err := pgxpool.Connect(context.Background(), dsn)
	if err != nil {
		return nil, err
	}
	return dbpool, nil
}



func main() {

	addr := flag.String("addr", ":4000", "HTTP network address")
	dsn := flag.String("DBDsn", "postgres://postgres:A27B@localhost:5432/snippetbox",
		"Connection string to DB")
	secret := flag.String("secret", "s6Ndh+pPbnzHbS*+9Pk8qGWhTzbpa@ge", "Secret key")

	flag.Parse()




	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)



	dbpool, err := OpenDB(*dsn)
	if err != nil {
		errorLog.Fatal(err)
	}
	defer dbpool.Close()

	templateCache, err := newTemplateCache("./ui/html/")
	if err != nil {
		errorLog.Fatal(err) }


	session := sessions.New([]byte(*secret))
	session.Lifetime = 12 * time.Hour


	app := &application{
		errorLog: errorLog,
		infoLog: infoLog,
		snippets: &postgresql.SnippetModel{DB: dbpool},
		templateCache: templateCache,
		session: session,
		users: &postgresql.UserModel{DB: dbpool},
	}


	tlsConfig := &tls.Config{
		PreferServerCipherSuites: true,
		CurvePreferences: []tls.CurveID{tls.X25519, tls.CurveP256},
	}

	srv := &http.Server{
		Addr: *addr,
		ErrorLog: errorLog,
		Handler: app.routes(),
		TLSConfig: tlsConfig,
		IdleTimeout: time.Minute,
		ReadTimeout: 5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}



	infoLog.Printf("Starting server on %s", *addr)
	err = srv.ListenAndServeTLS("./tls/cert.pem", "./tls/key.pem")
	errorLog.Fatal(err)
}