package main

import (
	"armanbimak27/pkg/forms"
	"armanbimak27/pkg/models"
)

type TemplateData struct {
	CurrentYear 	int
	Snippet     	*models.Snippet
	Flash 			string
	IsAuthenticated bool
	Form  			*forms.Form
	Snippets 		[]*models.Snippet
}