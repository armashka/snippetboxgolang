module armanbimak27

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/golangcollege/sessions v1.2.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/justinas/alice v1.2.0
	github.com/pkg/errors v0.8.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
