package postgresql

import (
	"armanbimak27/pkg/models"
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"time"
)


type SnippetModel struct {
	DB *pgxpool.Pool
}

func (m *SnippetModel) Insert(title, content string, expires int) (int, error) {
	stmt := `INSERT INTO snippets (title,content,created,expires) 
	VALUES($1, $2 , $3, $4)  RETURNING id;`


	var id int64
	err := m.DB.QueryRow(context.Background(), stmt, title, content, time.Now(), time.Now().
		AddDate(0,0,expires)).Scan(&id)
	if err != nil {
		return -1, err
	}

	return int(id), nil
}

func (m *SnippetModel) Get(id int) (*models.Snippet, error) {

	stmt := `SELECT id, title, content, created, expires FROM snippets
	WHERE expires > current_timestamp 
	AND id = $1`

	row := m.DB.QueryRow(context.Background(),stmt, id)
	s := &models.Snippet{}
	err := row.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
	if err != nil {

		if errors.Is(err, pgx.ErrNoRows) {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}

	return s, nil
}


func (m *SnippetModel) Latest() ([]*models.Snippet, error) {

	stmt := `SELECT id, title, content, created, expires FROM snippets 
		WHERE expires > $1 ORDER BY created DESC LIMIT 10`

	rows, err := m.DB.Query(context.Background(),stmt, time.Now())
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var snippets []*models.Snippet
	for rows.Next() {
		s := &models.Snippet{}

		err = rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if err != nil {
			return nil, err }
		snippets = append(snippets, s) }

	if err = rows.Err(); err != nil { return nil, err
	}
	return snippets, nil
}